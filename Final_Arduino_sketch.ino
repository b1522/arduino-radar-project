#include<Servo.h>

int trigPin =10;
int echoPin=11;
int buzzPin=8;
long duration;
int distance;
Servo myservo;

void setup() 
{
  // put your setup code here, to run once:
pinMode(trigPin,OUTPUT);
pinMode(echoPin,INPUT);
pinMode(buzzPin,OUTPUT);
myservo.attach(12);
Serial.begin(9600);

}

void loop() 
{
  // put your main code here, to run repeatedly:
for(int i=0;i<180;i++)
{
  myservo.write(i);
  delay(30);
  distance = calc_dis();
  Serial.print(i);
  Serial.print(",");
  Serial.print(distance);
  Serial.print(".");
  if (calc_dis()<=50 && calc_dis()>=0)
  {
    digitalWrite(buzzPin,HIGH);
    delay(5);
    digitalWrite(buzzPin,LOW);
  }
  
}
for(int i=180;i>0;i--)
{
  myservo.write(i);
  delay(30);
  distance = calc_dis();
  Serial.print(i);
  Serial.print(",");
  Serial.print(distance);
  Serial.print(".");
  if (calc_dis()<=50 && calc_dis()>=0)
  {
    digitalWrite(buzzPin,HIGH);
    delay(5);
    digitalWrite(buzzPin,LOW);
  }
  
}
delay(60);

}
int calc_dis()
{
  digitalWrite(trigPin, LOW);
  delay(2);
  digitalWrite(trigPin,HIGH);
  delay(10);
  digitalWrite(trigPin,LOW);
  duration = pulseIn(echoPin,HIGH);
  distance = duration*0.034/2;
  return distance;
}
